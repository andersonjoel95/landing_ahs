const modal1 = document.getElementById("simpleModal"); // Obtiene el valor del modal

// Evento que escucha el click el modal para cerrar el modal y detener el video
modal1.addEventListener("click", detener);

// Load the IFrame Player API code asynchronously.
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


// Replace the 'ytplayer' element with an <iframe> and
// YouTube player after the API code downloads.
var player;

function onYouTubePlayerAPIReady() {
    player = new YT.Player('ytplayer', {
        height: '420',
        width: '720',
        videoId: 'I6kfin-UeAQ',
        events: {
            // 'onReady': onPlayerReady,
            // 'onStateChange': onPlayerStateChange
        }
    });
}

// Funcion que pausa el video cuando el modal es "none"
function detener(event) {


    if (event.target.getAttribute("class") === "modalActive modal") {

        player.pauseVideo();

    }
}