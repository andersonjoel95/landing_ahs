const contenedorSpan = document.getElementById("letterAnimation");
const cursorSpan = document.querySelector(".cursor");

const arregloTexto = ["cultures", "countries", "opportunities"];
const tipeoDelay = 200; //tiempo de espera en escribir el siguiente caracter
const borradoDelay = 100; //tiempo de espera al borrar un caracter
const newTextDelay = 2000; // retraso de tiempo entre strings
let textArregloIndex = 0; // Arreglo que contiene un string
let charIndex = 0; //Ubicacion de cada caracter del arreglo



function inicio() {

    //Evento DOM llama la funcion al cargar la pagina con un retraso de 2 segundos
    document.addEventListener("DOMContentLoaded", function() {

        setTimeout(escribir, newTextDelay);
    });

}

//Funcion para escribir el string
function escribir() {

    //Si el indice de caracter es menor al tamaño del arreglo, escribir 
    if (charIndex < arregloTexto[textArregloIndex].length) {

        //Si el cursor no contiene la clase typing, agregarla para que no parpadee
        if (!cursorSpan.classList.contains("typing")) {

            cursorSpan.classList.add("typing");

        }

        //Si contiene espacios, imprimir el espacio y el caracter siguiente seguido y aumentar el contador en 2
        if (arregloTexto[textArregloIndex].charAt(charIndex) === " ") {

            arregloTexto[textArregloIndex].charAt(charIndex).style.color = "blue";
            //Agregar los caracteres al contenedor SPAN en el DOM uno por uno segun indice charIndex
            contenedorSpan.textContent += arregloTexto[textArregloIndex].charAt(charIndex);
            contenedorSpan.textContent += arregloTexto[textArregloIndex].charAt(charIndex + 1);

            charIndex += 2;

        } else {

            contenedorSpan.textContent += arregloTexto[textArregloIndex].charAt(charIndex);
            charIndex++;
        }

        setTimeout(escribir, tipeoDelay);

    } else {

        cursorSpan.classList.remove("typing"); //remover clase typing y vuelve a parpadear cursor
        setTimeout(borrar, newTextDelay);
    }

}


//Funcion para borrar el string
function borrar() {

    if (charIndex > 0) {

        if (!cursorSpan.classList.contains("typing")) {

            cursorSpan.classList.add("typing");

        }

        //Asignar al contenedor un arreglo de texto desde el elemento 0 hasta el length-1 no incluye el ultimo
        // Ejemplo: del array [hard] solo tomo los tres primeros ---> [har] , es decir
        //se borra el ultimo, luego se reduce el contador y continuaria: [ha], [h]...
        contenedorSpan.textContent = arregloTexto[textArregloIndex].slice(0, charIndex - 1);
        charIndex--;

        setTimeout(borrar, borradoDelay);

    } else {

        cursorSpan.classList.remove("typing");

        //Aumento el indice del arreglo de strings para pasar al siguiente string
        textArregloIndex++;

        //Si el indice es mayor o igual al tamaño del arreglo se resetea a 0 su valor
        if (textArregloIndex >= arregloTexto.length) {

            textArregloIndex = 0;

        }

        setTimeout(escribir, tipeoDelay + 1000);
    }
}


inicio();