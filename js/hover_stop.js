//Variable global con valor del setInterval
var stopInterval;

var dataList = {

    watchVideo: document.querySelector(".boton-video"),
    watchIcon: document.querySelector(".icon-play"),
    watchTriangle: document.querySelector(".triangle"),
    formatearLoop: false,
    velocidadSlide: 5000

};

var dataOperation = {

    inicio: function() {

        //Eventos onmouseover y onmouseout 
        dataList.watchVideo.addEventListener("mouseover", dataOperation.detenerAnima);
        dataList.watchVideo.addEventListener("mouseout", dataOperation.iniciarAnima);

        dataList.watchIcon.addEventListener("mouseover", dataOperation.detenerAnima);
        dataList.watchIcon.addEventListener("mouseout", dataOperation.iniciarAnima);

        //Ejecutar la funcion setIntervalo
        dataOperation.intervalo();

    },


    cambios: function() {

        // Si es visible la caja con WATCH VIDEO, ocultarla y mostrar el boton PLAY
        if (dataList.watchVideo.classList.contains("boton-video")) {

            setTimeout(function() {

                dataList.watchVideo.style.display = "none";

            }, 350);


            setTimeout(function() {

                dataList.watchIcon.classList.add("icon-playActivar");

            }, 350);

        }

        //Si el boton PLAY es visible, ocultarlo y mostrar la caja WATCH VIDEO
        if (dataList.watchVideo.style.display == "none") {

            setTimeout(function() {

                dataList.watchVideo.style.display = "block";

            }, 350);

            setTimeout(function() {

                dataList.watchIcon.classList.remove("icon-playActivar");

            }, 350);

        }

    },

    // Funcion que contiene el setInterval o bucle
    intervalo: function() {

        //Asignar el valor del setInterval a la variable global
        stopInterval = setInterval(function() {

            dataOperation.cambios();

        }, dataList.velocidadSlide);

    },

    //Iniciar la animacion una se aleje el mouse de la zona
    iniciarAnima: function() {

        dataOperation.intervalo();

    },

    //Detener la animacion una vez se acerca el mouse sobre la zona 
    detenerAnima: function() {

        clearInterval(stopInterval);

    }

};


dataOperation.inicio();