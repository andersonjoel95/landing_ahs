// Variables

const imagenSection = document.getElementsByClassName("online")[0];
const textoSection = document.getElementsByClassName("online-texto")[0];
const cajaScrollSection2 = document.getElementById("section2");
const cajaScrollSection4 = document.getElementById("section4");
const cajaScrollNiveles = document.getElementById("challenge");
const imagenDesign = document.querySelector(".design");
const textoDesign = document.querySelector(".design-text");
const imagenNiveles = document.getElementsByClassName("container-challenge")[0];
var posicionScroll = 0;
let dimensionMovil = 0;

// Capturar el evento scrolcajaScrollSection2
eventListenersScroll();

function eventListenersScroll() {

    dimensionMovil = screen.width;

    if (dimensionMovil > 1024) {
        document.addEventListener("scroll", efectoScroll);
    }

}

//Funcion efectoScroll

function efectoScroll() {

    posicionScroll = window.pageYOffset;

    if (posicionScroll >= cajaScrollSection2.offsetTop - 300) {

        imagenSection.classList.remove("online");
        imagenSection.classList.add("online-activar");

        textoSection.classList.remove("online-texto");
        textoSection.classList.add("online-texto-activar");

    }


    if (posicionScroll > cajaScrollSection4.offsetTop - 300) {

        imagenDesign.classList.remove("design");
        imagenDesign.classList.add("design-activar");

        textoDesign.classList.remove("design-text");
        textoDesign.classList.add("design-text-activar");

    }

    if (posicionScroll > cajaScrollNiveles.offsetTop - 300 && dimensionMovil > 1024) {


        imagenNiveles.classList.remove("container-challenge");
        imagenNiveles.classList.add("container-challenge-activar");



    }
}