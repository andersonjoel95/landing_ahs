// Variables

let paginacion = document.querySelectorAll("#paginacion span");
let cajaSlide = document.querySelector(".content-teacher-logo");
let teacher1 = document.querySelector(".teacher1");
let teacher2 = document.querySelector(".teacher2");
let teacher3 = document.querySelector(".teacher3");
let item = 0;
let intervaloSlide = 0;
let slideAuto = false;


// Funcion que inicia el script

function inicio() {

    for (let i = 0; i < paginacion.length; i++) {

        paginacion[i].addEventListener("click", paginacionSlide);

    }

    intervaloSlide();

}



function paginacionSlide(event) {

    // Obtener el indice de cada circulo 

    item = event.target.parentNode.getAttribute("item") - 1;
    operacionSlide(item);

}



function operacionSlide(event) {

    //Retardar 200ms la opacidad de los botones
    setTimeout(function() {

        //Reducir la opaciodad de los indices de paginacion a .5
        for (let i = 0; i < paginacion.length; i++) {

            paginacion[i].style.opacity = 0.5;
        }

        paginacion[item].style.opacity = 1; //solo mantener en 1 el actual

    }, 200);


    // Condicionales dependiendo del valor de la variables pasada a operacionSlide
    if (event === 0) {

        setTimeout(function() {

            teacher2.classList.remove("teacher-active");
            teacher3.classList.remove("teacher-active");

        }, 350);

        setTimeout(function() {

            teacher1.classList.remove("teacher-ocultar");

        }, 350);

    }


    if (event === 1) {

        setTimeout(function() {

            teacher2.classList.add("teacher-active");

        }, 350);

        setTimeout(function() {

            teacher1.classList.add("teacher-ocultar");
            teacher3.classList.remove("teacher-active");

        }, 350);

    }


    if (event === 2) {

        setTimeout(function() {

            teacher1.classList.add("teacher-ocultar");
            teacher2.classList.remove("teacher-active");

        }, 350);

        setTimeout(function() {

            teacher3.classList.add("teacher-active");

        }, 350);

    }

}

function botonNext() {

    //Si el valor del item es igual al último-1 del arreglo quiere decir que es el
    //último diapositiva, volverlo 0 es decir al inicio cuando se de en siguiente
    if (item == 2) {

        item = 0;

    } else {

        item++;

    }

    operacionSlide(item);
}


// Funcion con intervalo de tiempo
intervaloSlide = function() {

    setInterval(function() {

        if (slideAuto) {

            slideAuto = false;

        } else {

            botonNext();

        }

    }, 5000);

};

// Ejecutar desde el inicio
inicio();