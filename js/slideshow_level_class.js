// Vairables

const contentSlide = document.querySelector(".niveles-movil-content");
let boxLevel = document.querySelectorAll(".niveles-movil ul li");
let sumar = 0;
let restar = 0;
const backArray = document.querySelector(".anterior");
const nextArray = document.querySelector(".siguiente");
let posicion = 0;
let intervaloLevel = 0;
let dimensionDesktop = 0;



// Eventos

function inicio() {

    nextArray.addEventListener("click", next);

    // Condicional para comprobar que existe clase
    if (backArray.classList.contains("anterior")) {
        backArray.addEventListener("click", back);
    }

    dimensionDesktop = screen.width;

    if (dimensionDesktop > 1024) {

        intervaloLevel();
    }


}

// Funcion con intervalo de tiempo
intervaloLevel = function() {

    setInterval(function() {

        next();

    }, 3000);

};



function next() {

    if (sumar == 0) {

        contentSlide.style.left = "0%";
    }

    setTimeout(function() {

        contentSlide.style.left = sumar + "%";
        contentSlide.style.transition = "1s left ease";

    }, 100);


    posicion = sumar; //Asignar el valor de suma a posicion
    sumar -= 100; // Disminuir suma en -100


    // Cuando se encuentra en la  última posicion,
    // resetear para pasar al primer cuadro
    if (sumar < -400) {

        sumar = 0;
    }


    // Suma < 0 --> segunda posicion hacer visible arrow back
    if (sumar < 0) {

        backArray.classList.remove("anterior");
        backArray.classList.add("regresar");

    }

    // Primera posicion-> hacer visible arrow next y ocultar arrow back
    if (posicion == -400) {

        backArray.classList.remove("regresar");
        backArray.classList.add("anterior");

        nextArray.classList.remove("avanzar");
        nextArray.classList.add("siguiente");
    }

    // Última posicion, ocultar arrow_next
    if (posicion == -300) {
        nextArray.classList.remove("siguiente");
        nextArray.classList.add("avanzar");
    }

}


function back() {

    // Si la posicion es menor a 100, es decir 0 se mueven los div's
    if (posicion < 100) {

        contentSlide.style.left = (posicion + restar) + "%";
        contentSlide.style.transition = "1s left ease";

        posicion += 100; // Aumentar la posicion en 100 en cada evento
        sumar += 100; // aumentar la suma en 100 para que este a la par con la posicion

    }

    // Condiciones se ejecutan con evento click en Boton_back

    // Cuando se retrocede desde ultima posicion, hacer visible arrow next
    if (posicion > -300) {
        nextArray.classList.remove("avanzar");
        nextArray.classList.add("siguiente");

    }

    // Cuando se encuentra en primera posicion ocultar arrow back
    if (posicion > 0) {
        backArray.classList.remove("regresar");
        backArray.classList.add("anterior");
    }

    console.log('======== CON EL BOTON ATRAS =======');
    console.log(sumar);
    console.log(posicion);

}


inicio();