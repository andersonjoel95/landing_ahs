const cajaTeacherImagen = document.querySelector('.teacher-logo');
const lineaSeparadora = document.querySelector('.line-section');
let posicionScrollTeacher = 0;
let dimensionPantalla = 0;

eventListeners();

function eventListeners() {

    dimensionPantalla = screen.width;

    if (dimensionPantalla < 1025) {
        document.addEventListener('scroll', animationCajaSeparacion);
    }

}

function animationCajaSeparacion() {


    posicionScrollTeacher = window.pageYOffset;

    if (posicionScrollTeacher >= cajaTeacherImagen.offsetTop - 100) {
        lineaSeparadora.classList.replace('line-section', 'line-section-activar');

    }
}