//Obtener elemento que cierra el modal
const modalFrench = document.getElementById("simpleModalFrench");
const modalEnglish = document.getElementById("simpleModalEnglish");
const modalPortuguese = document.getElementById("simpleModalPortuguese");
const modalChinese = document.getElementById("simpleModalChinese");


const carteSite = document.querySelector(".modal-content");

//Obtener elemento que abre modal
const openModalFrench = document.querySelectorAll(".courses ul li");
// const openPlayBtn = document.querySelector(".icon-play");
// const openTriangleBtn = document.querySelector(".triangle");

cargarEventListeners();

function cargarEventListeners() {
    //Eventos click para abrir el modal
    openModalFrench.forEach(function(openModalFrancia) {
        openModalFrancia.addEventListener("click", openModal);

    });

    //Eventos click para cerrar el modal
    modalFrench.addEventListener("click", closeModal);
    modalEnglish.addEventListener("click", closeModal);
    modalPortuguese.addEventListener("click", closeModal);
    modalChinese.addEventListener("click", closeModal);

}


//Funcion abrir el modal
function openModal(e) {

    let cursoValor;
    cursoValor = e.target.parentElement.getAttribute('data-id');

    switch (cursoValor) {

        case 'french':
            modalFrench.classList.remove("modal");
            modalFrench.classList.add("modalActive");
            break;

        case 'english':
            modalEnglish.classList.remove("modal");
            modalEnglish.classList.add("modalActive");
            break;

        case 'portuguese':
            modalPortuguese.classList.remove("modal");
            modalPortuguese.classList.add("modalActive");
            break;

        case 'chinese':
            modalChinese.classList.remove("modal");
            modalChinese.classList.add("modalActive");
            break;
    }
}

//Funcio cerrar el modal
function closeModal(e) {

    let valorModalParent;
    if (e.target.getAttribute("class") === "modalActive") {

        valorModalParent = e.target.getAttribute('id');

        switch (valorModalParent) {

            case 'simpleModalFrench':

                modalFrench.classList.add("modal");

                setTimeout(function() {
                    modalFrench.classList.remove("modalActive");
                }, 200);
                break;


            case 'simpleModalEnglish':

                modalEnglish.classList.add("modal");

                setTimeout(function() {
                    modalEnglish.classList.remove("modalActive");
                }, 200);
                break;


            case 'simpleModalPortuguese':

                modalPortuguese.classList.add("modal");

                setTimeout(function() {
                    modalPortuguese.classList.remove("modalActive");
                }, 200);
                break;

            case 'simpleModalChinese':

                modalChinese.classList.add("modal");

                setTimeout(function() {
                    modalChinese.classList.remove("modalActive");
                }, 200);
                break;
        }

    }

}