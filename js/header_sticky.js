//objeto con valores
var p = {
    barraMenu: document.getElementById("cabezera"),
    distancia: null
};


//objeto con metodos
var m = {
    inicio: function() {
        document.addEventListener("scroll", m.calcularPixeles);
    },

    calcularPixeles: function() {
        if (window.scrollY > 0) {
            p.barraMenu.style.opacity = "1";
        } else {
            p.barraMenu.style.opacity = "1";

        }
    }

};

m.inicio();