// Variables

const imagenSection = document.getElementById("online");
const textoSection = document.getElementsByClassName("online-texto")[0];
const cajaScrollSection2 = document.getElementById("section2");
const cajaScrollSection4 = document.getElementById("section4");
const imagenDesign = document.getElementById("design");
const textoDesign = document.getElementById("design-text");
var posicionScroll = 0;

// Capturar el evento scrolcajaScrollSection2
document.addEventListener("scroll", efectoScroll);

//Funcion efectoScroll

function efectoScroll() {

    posicionScroll = window.pageYOffset;


    if (posicionScroll > cajaScrollSection2.offsetTop - 200) {

        imagenSection.style.marginLeft = "0%";
        textoSection.style.right = "0%";

    } else if (cajaScrollSection2.offsetTop > posicionScroll && imagenSection.style.marginLeft < "0%" || posicionScroll === 0) {

        imagenSection.style.marginLeft = posicionScroll / 6.8 - 100 + "%";
        textoSection.style.right = posicionScroll / 6.8 - 100 + "%";

    }


    if (posicionScroll > cajaScrollSection4.offsetTop - 100) {

        imagenDesign.style.marginLeft = "0%";
        textoDesign.style.right = "0%";

    } else if (cajaScrollSection4.offsetTop > posicionScroll && imagenDesign.style.marginLeft < "0%" || posicionScroll < 1700) {


        imagenDesign.style.marginLeft = posicionScroll / 24.5 - 100 + "%";
        textoDesign.style.right = posicionScroll / 24.5 - 100 + "%";


    }
}