// Variables

var opcion = document.querySelectorAll(".navegador a");
const contenido = document.getElementById("content");
const profesor = document.getElementById("teacher");

var posicionScrollBotonera = 0;
var contar = 0;

var tiempo = null;
var ruta = null;
var intervalo = null;
var destinoScroll = null;




// Captura del evento click

for (var k = 0; k < opcion.length; k++) {

    opcion[k].addEventListener("click", elegir);

}


// Funcion desplazamiento

function elegir(event) {

    event.preventDefault();

    // Obtener el nombre de la ruta
    ruta = event.target.getAttribute("href");
    destinoScroll = document.querySelector(ruta).offsetTop; // Obtener la medida de inicio de la caja destino

    // Resetear el valor de posicionScroll
    posicionScrollBotonera = 0;

    intervalo = setInterval(function() {

        // Scrollear de 100 en 100
        if (posicionScrollBotonera < destinoScroll) {

            posicionScrollBotonera += 2;

            setTimeout(function() {

                posicionScrollBotonera += 25;


                // Detenerse si llega a la caja pedida
                if (posicionScrollBotonera >= destinoScroll) {

                    posicionScrollBotonera = destinoScroll;
                    clearInterval(intervalo);

                }

            }, 500);

        }

        window.scrollTo(0, posicionScrollBotonera); // Dirigirse al top de la caja, donde empieza  la caja

    }, 3);






}

// Evento contador de click para manipular el salto desigual al navegar por boton
// cuando se carga por primera vez la pagina y las figuras flotantes no aparecen