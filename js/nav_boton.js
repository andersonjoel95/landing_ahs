// Variables
const botonera1 = document.getElementsByClassName("opc1")[0];

const botonera2 = document.querySelector("header .opc2");
const botonera3 = document.querySelector(".opc3");
const botonera4 = document.querySelector(".opc4");
const contenedor = document.getElementById("content");
const teacher = document.getElementById("teacher");

var posicionScrollBotonera = 0;
var contador = 0;

var ruta = null;
var intervalo = null;
var destinoScroll = null;




// Captura del evento click

botonera1.addEventListener("click", desplazamiento, false);
botonera2.addEventListener("click", desplazamiento, false);
botonera3.addEventListener("click", desplazamiento, false);
botonera4.addEventListener("click", desplazamiento, false);



// Funcion desplazamiento

function desplazamiento(ruta) {


    ruta.preventDefault();

    // Obtener el nombre de la ruta
    ruta = ruta.target.getAttribute("href");
    destinoScroll = document.querySelector(ruta).offsetTop; // Obtener la medida de inicio de la caja destino

    // Resetear el valor de posicionScroll
    posicionScrollBotonera = 0;

    // Comentario que controla el salto forzado al no carga las imagenes
    // la primera vez la pagina

    /*
    //Iniciar el contador de clicks
    contarClick();

    if (contador === 1) {


        intervalo = setInterval(function() {

            // Scrollear de 100 en 100
            if (posicionScrollBotonera < destinoScroll) {

                posicionScrollBotonera += 3;

                setTimeout(function() {

                    posicionScrollBotonera += 50;

                    // Detenerse si llega a la caja pedida
                    if (posicionScrollBotonera >= destinoScroll) {

                        posicionScrollBotonera = destinoScroll;
                        clearInterval(intervalo);

                    }

                }, 400);



            }

            window.scrollTo(0, posicionScrollBotonera); // Dirigirse al top de la caja, donde empieza  la caja

        }, 10);




    } else{

    }

    */


    intervalo = setInterval(function() {

        // Scrollear de 100 en 100
        if (posicionScrollBotonera < destinoScroll) {

            posicionScrollBotonera += 2;

            setTimeout(function() {
                posicionScrollBotonera += 30;

                // Detenerse si llega a la caja pedida
                if (posicionScrollBotonera >= destinoScroll - 100) {

                    posicionScrollBotonera = destinoScroll - 100;
                    clearInterval(intervalo);

                }

            }, 500);

        }

        window.scrollTo(0, posicionScrollBotonera);

    }, 5);



}

// Evento contador de click para manipular el salto desigual al navegar por boton
// cuando se carga por primera vez la pagina y las figuras flotantes no aparecen

var contarClick = () => {
    if (botonera3 || botonera1 || botonera2 || botonera4) {
        contador++;
    }
    return contador;
};





/*
// Si se quiere retornar a una seccion que esta encima de la posicion actual
else {

    posicionScrollBotonera -= 200;

    // Detenerse si llega a la caja pedida
    if (posicionScrollBotonera <= destinoScroll) {

        posicionScrollBotonera = destinoScroll;
        clearInterval(intervalo);

    }

}

*/