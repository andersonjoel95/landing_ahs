//Obtener elemento que cierra el modal
const modal = document.getElementById("simpleModal");
const video = document.getElementById(".modal-content iframe");

//Obtener elemento que abre modal
const openWatchBtn = document.querySelector(".boton-video");
// const openPlayBtn = document.querySelector(".icon-play");
// const openTriangleBtn = document.querySelector(".triangle");

//Eventos click para abrir el modal
openWatchBtn.addEventListener("click", openModal);
// openPlayBtn.addEventListener("click", openModal);
// openTriangleBtn.addEventListener("click", openModal);

//Eventos click para cerrar el modal
modal.addEventListener("click", closeModal);


//Funcion abrir el modal
function openModal() {

    modal.classList.remove("modal");
    modal.classList.add("modalActive");

}

//Funcio cerrar el modal
function closeModal(event) {

    if (event.target.getAttribute("class") === "modalActive") {



        modal.classList.add("modal");

        setTimeout(function() {
            modal.classList.remove("modalActive");

        }, 200);

        // modal.style.display = "none";

    }


}