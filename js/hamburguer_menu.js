// Variables

const menu = document.querySelector(".menu");
const opciones = document.querySelector(".navegador");

// Evento click

menu.addEventListener("click", desplegar);

// Funcion desplegar

function desplegar() {

    menu.classList.toggle("change");

    opciones.classList.toggle("navegar-activo");

}