const esferaNaranja = document.querySelector(".esfera-naranja");
const esferaVerde = document.querySelector(".esfera-verde");
const esferaMorada1 = document.querySelector(".esfera-morada1");
const esferaMorada2 = document.querySelector(".esfera-morada2");
const esferaAzul = document.querySelector(".esfera-azul");
const esferaLila1 = document.querySelector(".esfera-lila1");
const esferaLila2 = document.querySelector(".esfera-lila2");
const esferaAmarilla = document.querySelector(".esfera-amarilla");
const esferaRoja1 = document.querySelector(".esfera-roja1");
const esferaRoja2 = document.querySelector(".esfera-roja2");

const esferaRoja1Movil = document.querySelector(".esfera-roja1-movil");
const esferaRoja2Movil = document.querySelector(".esfera-roja2-movil");

const esferaCeleste = document.querySelector(".esfera-celeste");
const esferaAzulMovil = document.querySelector(".esfera-azul-movil");
const esferaMoradaMovil = document.querySelector(".esfera-morada-movil");
const esferaLila1Movil = document.querySelector(".esfera-lila1-movil");
const esferaLila2Movil = document.querySelector(".esfera-lila2-movil");

const contentTeacher = document.querySelector(".teacher-logo");
const lineaTeacher = document.querySelector(".comodin-teacher");
const caracterTeacher = document.querySelector(".teacher-car");

var dimensionPantallaMovil = 0;
window.addEventListener("scroll", function() {

    setTimeout(function() {

        americanParallax();
    }, 200);
});


var americanParallax = () => {

    // aux.style.transform = "translate3d(0px, 150px, 0px)";


    dimensionPantallaMovil = screen.width;

    if (dimensionPantallaMovil > 1024) {

        var scrolled = window.pageYOffset;

        var rateY = scrolled * 0.1;
        var rateX = scrolled * 0.05;


        esferaNaranja.style.transform = "translate3d(0px, " + rateY + "px, 0px)";
        esferaVerde.style.transform = "translate3d(" + -rateX + "px, " + rateY + "px, 0px)";
        esferaMorada1.style.transform = "translate3d(0px, " + rateX + "px , 0px)";
        esferaMorada2.style.transform = "translate3d(0px, " + rateY + "px, 0px)";
        esferaLila1.style.transform = "translate3d(" + rateX / 2 + "px, " + rateY + "px, 0px)";
        esferaLila2.style.transform = "translate3d(0px, " + rateY + "px, 0px)";
        esferaAzul.style.transform = "translate3d(" + rateX / 2 + "px, " + rateY + "px, 0px)";
        esferaRoja1.style.transform = "translate3d(0px, " + rateY / 1.8 + "px, 0px)";
        esferaRoja2.style.transform = "translate3d(" + -rateX + "px, " + rateY / 1.5 + "px, 0px)";
        esferaAmarilla.style.transform = "translate3d(" + -rateX + "px, " + rateY + "px, 0px)";
        esferaCeleste.style.transform = "translate3d(" + -rateX / 2 + "px, " + rateY * 2 + "px, 0px)";
        esferaAzulMovil.style.transform = "translate3d(0px, " + rateY / 2 + "px, 0px)";
        esferaRoja1Movil.style.transform = "translate3d(0px, " + rateY / 2 + "px, 0px)";
        esferaRoja2Movil.style.transform = "translate3d(0px, " + rateY / 2 + "px, 0px)";
        esferaMoradaMovil.style.transform = "translate3d(0px, " + rateY / 2 + "px, 0px)";
        esferaLila1Movil.style.transform = "translate3d(0px, " + rateY / 2 + "px, 0px)";
        esferaLila2Movil.style.transform = "translate3d(0px, " + rateY / 2 + "px, 0px)";

        // contentTeacher.style.transform = 'translate3d(0px,' + rateY / 3 + 'px, 0px)';

        // caracterTeacher.style.transform = 'translate3d(0px,' + rateY / 3 + 'px, 0px)';

    }

};